/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.panels.navigation;

import org.bigsql.sqlstudio.client.models.DatabaseObjectInfo;

public interface MenuPanel {

	public void setSchema(DatabaseObjectInfo schema);
	
	public void refresh();

	public Boolean selectFirst();
}
