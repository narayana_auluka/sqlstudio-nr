/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.panels;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.bigsql.sqlstudio.client.SqlStudio;
import org.bigsql.sqlstudio.client.SqlStudio.ITEM_OBJECT_TYPE;
import org.bigsql.sqlstudio.client.models.ModelInfo;
import org.bigsql.sqlstudio.client.models.RuleInfo;
import org.bigsql.sqlstudio.client.panels.popups.AddRulePopUp;
import org.bigsql.sqlstudio.client.panels.popups.DropItemObjectPopUp;
import org.bigsql.sqlstudio.client.panels.popups.PopUpException;
import org.bigsql.sqlstudio.client.providers.RuleListDataProvider;

public class RulePanel extends Composite implements DetailsPanel {
	
	private static interface GetValue<C> {
	    C getValue(RuleInfo column);
	}

	private DataGrid<RuleInfo> dataGrid;
    private RuleListDataProvider dataProvider = new RuleListDataProvider();

    private TextArea ruleDef;
    
    private ModelInfo item = null;
    
    private final SingleSelectionModel<RuleInfo> selectionModel = 
        	new SingleSelectionModel<RuleInfo>(RuleInfo.KEY_PROVIDER);

	private String MAIN_HEIGHT = "250px";
	
	public static int ROWS_PER_PAGE = 5;
	public static int MAX_RULES = 1600;
	
	public void setItem(ModelInfo item) {
		this.item = item;
		
		ruleDef.setText("");
		dataProvider.setItem(item.getSchema(), item.getId(), item.getItemType());
	}
	
	public RulePanel() {
		
		VerticalPanel panel = new VerticalPanel();

		panel.add(getButtonBar());
		panel.add(getMainPanel());
		panel.add(getDetailSection());
				
		initWidget(panel);
	}

	private Widget getButtonBar() {
		HorizontalPanel bar = new HorizontalPanel();
		
		PushButton refresh = getRefreshButton();
		PushButton drop = getDropButton();
		PushButton create = getCreateButton();
		
		bar.add(refresh);
		bar.add(drop);
		bar.add(create);
		
		return bar.asWidget();
	}
	
	private PushButton getRefreshButton() {
		PushButton button = new PushButton(new Image(SqlStudio.Images.refresh()));
		button.setTitle("Refresh");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				refresh();				
			}			
		});
		return button;
	}
	
	private PushButton getDropButton() {
		PushButton button = new PushButton(new Image(SqlStudio.Images.drop()));
		button.setTitle("Drop RUle");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (selectionModel.getSelectedObject() != null
						&& !"".equals(selectionModel.getSelectedObject()
								.getName())) {
					
					DropItemObjectPopUp pop = new DropItemObjectPopUp();
					pop.setSelectionModel(selectionModel);
					pop.setDataProvider(dataProvider);
					pop.setItem(item);
					pop.setObject(selectionModel.getSelectedObject().getName());
					pop.setObjectType(ITEM_OBJECT_TYPE.RULE);
					try {
						pop.getDialogBox();
					} catch (PopUpException caught) {
						Window.alert(caught.getMessage());
					}
				}
			}
		});
		return button;
	}

	private PushButton getCreateButton() {
		PushButton button = new PushButton(new Image(SqlStudio.Images.create()));
		button.setTitle("Create Rule");

		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				AddRulePopUp pop = new AddRulePopUp();
				pop.setSelectionModel(selectionModel);
				pop.setDataProvider(dataProvider);
				pop.setItem(item);
				try {
					pop.getDialogBox();
				} catch (PopUpException caught) {
					Window.alert(caught.getMessage());
				}
				pop.setupCodePanel();
			}
		});

		return button;
	}

	private Widget getMainPanel() {
		SimplePanel panel = new SimplePanel();
		panel.setWidth("100%");
		panel.setHeight("100%");
		
		dataGrid = new DataGrid<RuleInfo>(MAX_RULES, RuleInfo.KEY_PROVIDER);
		dataGrid.setHeight(MAIN_HEIGHT);
		
		Column<RuleInfo, String> columnName = addColumn(new TextCell(), "Rule Name", new GetValue<String>() {
	        public String getValue(RuleInfo rule) {
	          return rule.getName();
	        }
	      }, null);

		
		Column<RuleInfo, String> ruleType = addColumn(new TextCell(), "Type", new GetValue<String>() {
	        public String getValue(RuleInfo rule) {
	          return rule.getType().toUpperCase();
	        }
	      }, null);

		Column<RuleInfo, ImageResource> ruleEnabled = addColumn(new ImageResourceCell(), "Enabled", new GetValue<ImageResource>() {
	        public ImageResource getValue(RuleInfo rule) {
	        	if (rule.isEnabled()) {
	        		return SqlStudio.Images.trueBox();
	        	}
	          return SqlStudio.Images.falseBox();
	        }
	      }, null);

		Column<RuleInfo, ImageResource> instead = addColumn(new ImageResourceCell(), "Instead", new GetValue<ImageResource>() {
	        public ImageResource getValue(RuleInfo rule) {
	        	if (rule.isInstead()) {
	        		return SqlStudio.Images.trueBox();
	        	}
        		return SqlStudio.Images.falseBox();
	        }
	      }, null);

		dataGrid.setColumnWidth(columnName, "200px");
		dataGrid.setColumnWidth(ruleType, "120px");
		dataGrid.setColumnWidth(ruleEnabled, "100px");
		dataGrid.setColumnWidth(instead, "100px");

		ruleType.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		ruleEnabled.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		instead.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);		

		dataGrid.setLoadingIndicator(new Image(SqlStudio.Images.spinner()));

		dataProvider.addDataDisplay(dataGrid);

		dataGrid.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler((new 
				SelectionChangeEvent.Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						RuleInfo rule = selectionModel.getSelectedObject();
						ruleDef.setText(rule.getDefinition());
					}			
		}));

		panel.add(dataGrid);
		
		return panel.asWidget();
	}
	

	private <C> Column<RuleInfo, C> addColumn(Cell<C> cell, String headerText,
		      final GetValue<C> getter, FieldUpdater<RuleInfo, C> fieldUpdater) {
		    Column<RuleInfo, C> column = new Column<RuleInfo, C>(cell) {
		      @Override
		      public C getValue(RuleInfo object) {
		        return getter.getValue(object);
		      }
		    };
		    column.setFieldUpdater(fieldUpdater);

		    dataGrid.addColumn(column, headerText);
		    return column;
	}

	private Widget getDetailSection() {
		HorizontalPanel panel = new HorizontalPanel();
		panel.setWidth("100%");
		panel.setStyleName("studio-Bottom-Panel");
				
		VerticalPanel ruleDefPanel = new VerticalPanel();
		ruleDefPanel.setWidth("95%");

		Label lbl = new Label();
		lbl.setText("Rule Definition");
		lbl.setStyleName("studio-Label-Small");

		ruleDef = new TextArea();
		ruleDef.setWidth("100%");
		ruleDef.setVisibleLines(5);
		ruleDef.setReadOnly(true);
		
		ruleDefPanel.add(lbl);
		ruleDefPanel.add(ruleDef);
		
		panel.add(ruleDefPanel);
				
		return panel.asWidget();
	}

	@Override
	public void refresh() {
		dataProvider.setItem(item.getSchema(), item.getId(), item.getItemType());
	}
}
