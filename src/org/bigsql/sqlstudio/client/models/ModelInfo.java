/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.models;

import org.bigsql.sqlstudio.client.SqlStudio.ITEM_TYPE;

public interface ModelInfo {
	public String getFullName();
	
	public String getName();
	
	public int getId();
	
	public ITEM_TYPE getItemType();
	
	public String getComment();
	
	public int getSchema();	
}
