/*
 * SQL Studio
 *
 * Copyright (c) 2014, BigSQL.
 * Portions Copyright (c) 2013 - 2014, Open Source Consulting Group, Inc.
 * Portions Copyright (c) 2012 - 2013, StormDB, Inc.
 *
 */
package org.bigsql.sqlstudio.client.handlers;

import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.bigsql.sqlstudio.client.SqlStudio;
import org.bigsql.sqlstudio.client.models.ModelInfo;

public class SelectionChangeHandler implements SelectionChangeEvent.Handler {

	private final SqlStudio main;

	public SelectionChangeHandler(SqlStudio main) {
    	this.main = main;
    }
	
    public void onSelectionChange(SelectionChangeEvent event) {
    	SqlStudio.showWaitCursor();    	
    	Object sender = event.getSource();
    	
    	if (sender instanceof SingleSelectionModel) {
    		SingleSelectionModel<?> selectionModel = (SingleSelectionModel<?>) sender;
        	ModelInfo selected = (ModelInfo) selectionModel.getSelectedObject();
  	    	main.setSelectedItem(selected);            

    	}
  }
}
